package jp.alhinc.ito_yuki.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {

	public static void main(String[] args){
		try{
			
			Map<String,String> branch = new HashMap<>();
			Map<String,Long> earnings = new HashMap<>();
			List<String> rcdFilename = new ArrayList<>();
			FilenameFilter filter = new FilenameFilter(){
				public boolean accept(File file, String str){
					if(str.matches("^\\d{8}.rcd") && new File(file,str).isFile()) {
						rcdFilename.add(str.substring(0, 8));
						return true;
					}
					
					return false;
				}
			};

			//支店定義ファイル読み込み(支店コード、支店名)
			File file = new File(args[0], "branch.lst");
			if (!file.exists()) {
				System.out.println("支店定義ファイルが存在しません");
				return;
			}
			
			BufferedReader br = null;
			try{
				br = new BufferedReader(new FileReader(file));
				String line;
				while((line = br.readLine()) != null){
					String[] branchList = line.split(",", -1);
					if(!branchList[0].matches("^\\d{3}") || branchList.length != 2){
						System.out.println("支店定義ファイルのフォーマットが不正です");
						return;
					}
					branch.put(branchList[0], branchList[1]);
					earnings.put(branchList[0], 0L);
				}
			}finally{
				if(br != null){
					br.close();
				}
			}
			//集計
			try{
				File[] files = new File(args[0]).listFiles(filter);
				for(int i = 0; i < files.length; i++){
					int currentNumber = Integer.parseInt(rcdFilename.get(i));
					if(i < files.length - 1){
						int nextNumber = Integer.parseInt(rcdFilename.get(i + 1));
						if(nextNumber - currentNumber != 1){
							System.out.println("売上ファイル名が連番になっていません");
							return;
						}
					}
					br = new BufferedReader(new FileReader(files[i]));

					String line1 = br.readLine();
					long line2 = Long.parseLong(br.readLine());
					if(br.readLine() != null){
						System.out.println(files[i].getName() + "のフォーマットが不正です");
						return;
					}
					if(!branch.containsKey(line1)){
						System.out.println(files[i].getName() + "の支店コードが不正です");
						return;
					}
					
					Long total = earnings.get(line1);
					total += line2;
					
					if(total.toString().length() > 10){
						System.out.println("合計金額が10桁を超えました");
						return;
					}
					earnings.put(line1, total);
				}

			}finally{
				if(br != null){
					br.close();
				}
			}
			//出力
			PrintWriter pw = null;
			try{
				File branchOut = new File(args[0], "branch.out");

				pw = new PrintWriter(new BufferedWriter(new FileWriter(branchOut)));
				for (String code : branch.keySet()) {
					pw.println(code + "," + 
						branch.get(code) + "," + 
						earnings.get(code));
				}
			}finally{
				if(pw != null){
					pw.close();
				}
			}
		}catch(Exception e){
			System.out.println("予期せぬエラーが発生しました");
		}
	}
}
